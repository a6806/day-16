import argparse
from typing import TextIO
from enum import Enum, auto
from math import prod


class Packet:
    def __init__(self, version, type_id):
        self.version = version
        self.type_id = type_id
        self.sub_packets = []

    def get_version(self):
        return self.version

    def get_type_id(self):
        return self.type_id

    def get_sub_packets(self):
        return self.sub_packets

    def process_bit(self, bit):
        raise NotImplementedError

    def get_version_sum(self):
        version_sum = self.version
        for sub in self.sub_packets:
            version_sum += sub.get_version_sum()
        return version_sum

    def evaluate(self):
        raise NotImplementedError


LITERAL_PACKET_TYPE_ID = 4


class LiteralPacketState(Enum):
    PREFIX = auto()
    INTERMEDIATE = auto()
    FINAL = auto()


class LiteralPacket(Packet):
    def __init__(self, version):
        super().__init__(version, LITERAL_PACKET_TYPE_ID)
        self.state = LiteralPacketState.PREFIX
        self.buffer = []
        self.value = None

    def __str__(self):
        return f'[Version {self.version}, Type ID {self.type_id}, Value {self.value}]'

    def process_bit(self, bit):
        next_state = self.state
        complete = False
        if self.state == LiteralPacketState.PREFIX:
            if bit == 1:
                next_state = LiteralPacketState.INTERMEDIATE
            else:
                next_state = LiteralPacketState.FINAL
        elif self.state == LiteralPacketState.INTERMEDIATE:
            self.buffer.append(bit)
            if len(self.buffer) % 4 == 0:
                next_state = LiteralPacketState.PREFIX
        elif self.state == LiteralPacketState.FINAL:
            self.buffer.append(bit)
            if len(self.buffer) % 4 == 0:
                self.value = bit_list_to_int(self.buffer)
                complete = True
                self.state = None

        self.state = next_state
        return complete

    def evaluate(self):
        return self.value


OPERATOR_PACKET_TYPE_ID_MAP = {
    0: sum,
    1: prod,
    2: min,
    3: max,
    5: lambda x: int(x[0] > x[1]),
    6: lambda x: int(x[0] < x[1]),
    7: lambda x: int(x[0] == x[1])
}


class OperatorPacketState(Enum):
    LENGTH_TYPE_ID = auto()
    TOTAL_LENGTH = auto()
    SUB_PACKET_COUNT = auto()
    TOTAL_LENGTH_PROCESS = auto()
    SUB_PACKET_PROCESS = auto()


class OperatorPacket(Packet):
    def __init__(self, version, type_id):
        super().__init__(version, type_id)
        self.buffer = []
        self.state = OperatorPacketState.LENGTH_TYPE_ID
        self.sub_parser = None
        self.total_length = None
        self.sub_packet_count = None
        self.total_length_so_far = None
        self.sub_packets_so_far = None

    def __str__(self):
        sub_packet_string = ','.join(self.sub_packets)
        return f'[Version {self.version}, Type ID {self.type_id}, Sub Packets {sub_packet_string}]'

    def evaluate(self):
        values = [p.evaluate() for p in self.sub_packets]
        return OPERATOR_PACKET_TYPE_ID_MAP[self.type_id](values)

    def process_bit(self, bit):
        complete = False
        next_state = self.state
        if self.state == OperatorPacketState.LENGTH_TYPE_ID:
            if bit == 1:
                next_state = OperatorPacketState.SUB_PACKET_COUNT
            else:
                next_state = OperatorPacketState.TOTAL_LENGTH
        elif self.state == OperatorPacketState.TOTAL_LENGTH:
            self.buffer.append(bit)
            if len(self.buffer) == 15:
                self.total_length = bit_list_to_int(self.buffer)
                self.buffer.clear()
                self.sub_parser = PacketParser()
                self.total_length_so_far = 0
                next_state = OperatorPacketState.TOTAL_LENGTH_PROCESS
        elif self.state == OperatorPacketState.SUB_PACKET_COUNT:
            self.buffer.append(bit)
            if len(self.buffer) == 11:
                self.sub_packet_count = bit_list_to_int(self.buffer)
                self.buffer.clear()
                self.sub_parser = PacketParser()
                self.sub_packets_so_far = 0
                next_state = OperatorPacketState.SUB_PACKET_PROCESS
        elif self.state == OperatorPacketState.TOTAL_LENGTH_PROCESS:
            packet = self.sub_parser.process_bit(bit)
            if packet is not None:
                self.sub_packets.append(packet)
                self.sub_parser = PacketParser()
            self.total_length_so_far += 1
            if self.total_length_so_far == self.total_length:
                complete = True
                next_state = None
        elif self.state == OperatorPacketState.SUB_PACKET_PROCESS:
            packet = self.sub_parser.process_bit(bit)
            if packet is not None:
                self.sub_packets.append(packet)
                self.sub_packets_so_far += 1
                self.sub_parser = PacketParser()
            if self.sub_packets_so_far == self.sub_packet_count:
                complete = True
                next_state = None

        self.state = next_state
        return complete


def bit_list_to_int(bit_list):
    out = 0
    for bit in bit_list:
        out = (out << 1) | bit
    return out


class PacketParserState(Enum):
    VERSION = auto()
    TYPE_ID = auto()
    PAYLOAD = auto()


class PacketParser:
    def __init__(self):
        self.state = PacketParserState.VERSION
        self.buffer = []
        self.version = None
        self.packet = None

    def process_bit(self, bit):
        next_state = self.state
        if self.state == PacketParserState.VERSION:
            self.buffer.append(bit)
            if len(self.buffer) == 3:
                self.version = bit_list_to_int(self.buffer)
                self.buffer.clear()
                next_state = PacketParserState.TYPE_ID
        elif self.state == PacketParserState.TYPE_ID:
            self.buffer.append(bit)
            if len(self.buffer) == 3:
                type_id = bit_list_to_int(self.buffer)
                self.buffer.clear()
                if type_id == LITERAL_PACKET_TYPE_ID:
                    self.packet = LiteralPacket(self.version)
                else:
                    self.packet = OperatorPacket(self.version, type_id)
                self.version = None
                next_state = PacketParserState.PAYLOAD
        elif self.state == PacketParserState.PAYLOAD:
            done = self.packet.process_bit(bit)
            if done:
                return self.packet

        self.state = next_state


HEX_TO_BITS_MAP = {
    '0': [0, 0, 0, 0],
    '1': [0, 0, 0, 1],
    '2': [0, 0, 1, 0],
    '3': [0, 0, 1, 1],
    '4': [0, 1, 0, 0],
    '5': [0, 1, 0, 1],
    '6': [0, 1, 1, 0],
    '7': [0, 1, 1, 1],
    '8': [1, 0, 0, 0],
    '9': [1, 0, 0, 1],
    'A': [1, 0, 1, 0],
    'B': [1, 0, 1, 1],
    'C': [1, 1, 0, 0],
    'D': [1, 1, 0, 1],
    'E': [1, 1, 1, 0],
    'F': [1, 1, 1, 1],
}


def hex_to_bit_list(hex_value):
    out = []
    for hex_bit in hex_value:
        out.extend(HEX_TO_BITS_MAP[hex_bit])
    return out


def part_1(input_file: TextIO):
    input_bits = hex_to_bit_list(input_file.read())
    parser = PacketParser()

    packet = None
    for bit in input_bits:
        packet = parser.process_bit(bit)
        if packet is not None:
            break
    if packet is None:
        raise RuntimeError('Failed to parse packet')

    return packet.get_version_sum()


def part_2(input_file: TextIO):
    input_bits = hex_to_bit_list(input_file.read())
    parser = PacketParser()

    packet = None
    for bit in input_bits:
        packet = parser.process_bit(bit)
        if packet is not None:
            break
    if packet is None:
        raise RuntimeError('Failed to parse packet')

    return packet.evaluate()


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 16')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
